import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
            'resources/sass/app.scss',
            'resources/js/app.js',
            'resoruces/saas/games.scss',
            'resources/sass/questions.scss',
            'resources/ts/questions.ts'
            ],
            refresh: true,
        }),
    ],
});
