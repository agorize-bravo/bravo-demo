<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\Subject;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(){

        $subjects = Subject::all();
        return view('games',compact('subjects'));
    }

    public function question($subject){
        $subj = Subject::whereId($subject)->first();
        $question =  Question::whereSubjectId($subject)->get()->random();
        return view('questions',compact('question'));
    }
}
