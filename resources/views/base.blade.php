<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bravo!</title>
    @vite('resources/sass/app.scss')
    @yield('styles')
    @yield('scripts')

</head>
<body>
    <body>
        <div class="grid">
            <div class="header"></div>
            @yield('body')

    </div>
</body>
</html>