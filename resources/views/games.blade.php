@extends('base')
@section('styles')
@vite('resources/sass/games.scss')
@endsection
@section('body')
  <div class="scroll-container">
    <div class="gridscroll">
      @foreach($subjects as $subject)
      <a href="/subjects/{{$subject->id}}" ><img src="{{ Vite::asset($subject->image) }}" ></a>
      @endforeach
    </div>
@endsection