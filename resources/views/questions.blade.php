@extends('base')
@section('styles')
@vite('resources/sass/questions.scss')
@endsection
@section('scripts')
@vite('resources/ts/questions.ts')
@endsection
@section('body')
<input type="hidden" class="rightElement" value="{{$question->correct_answer}}">
<h1 class="question">{{$question->question}}</h1>
<button class="answer a"  data-letter="a" >{{$question->a}}</button>
<button class="answer b"  data-letter="b">{{$question->b}}</button>
<button class="answer c"  data-letter="c">{{$question->c}}</button>
<button class="answer d"  data-letter="d">{{$question->d}}</button>
<div class="win hidden">
    <div class="container">
    <h1 class="correct">Bravo!</h1>
    <button class="next"> Next question!</button>
    <a href="/">Exit</a>

    </div>
</div>
<div class="lost hidden">
    <div class="container">
    <h1 class="correct">not Bravo!</h1>
    <button class="next"> Next question!</button>
    <h1><a href="/">Exit</a></h1>
    </div>
</div>
@endsection